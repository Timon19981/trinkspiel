import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { EditorPage } from '../editor/editor';
import { ChooseGamePage } from '../choose-game/choose-game';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  start: string = 'Lasst uns saufen!';
  createGame: string = 'Eigenes Spiel erstellen';

  editorPage: any;
  chooseGamePage: any;

  constructor(public navCtrl: NavController) {
    this.editorPage = EditorPage;
    this.chooseGamePage = ChooseGamePage;
  }

}
