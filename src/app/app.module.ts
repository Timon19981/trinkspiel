// modules/plugins
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ScreenOrientation } from '@ionic-native/screen-orientation'

// pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ChooseGamePage } from '../pages/choose-game/choose-game';
import { GamePage } from '../pages/game/game';
import { EditorPage } from '../pages/editor/editor';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ChooseGamePage,
    GamePage,
    EditorPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    EditorPage,
    ChooseGamePage,
    GamePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ScreenOrientation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
